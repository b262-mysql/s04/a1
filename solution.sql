
-- Find all artists that have letter d in its name.
SELECT * FROM artists WHERE name LIKE "%d%" OR "d%" OR "%d";

-- Find all songs that have a length of less than 3:30. 
SELECT * FROM songs WHERE length < "00:03:30"

-- Join the 'albums' and 'songs' tables. (Only show the album name, song name, and song length.)
SELECT albums.album_title , songs.song_name, songs.length FROM albums
   JOIN songs ON albums.id = songs.album_id;

-- Join the 'artists' and 'albums' tables. (Find all albums that has letter a in its name.) 
SELECT * FROM artists
   JOIN albums ON artists.id = albums.artist_id 
   WHERE albums.album_title LIKE "%a%";

-- Sort the albums in Z-A order. (Show only the first 4 records.)
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

-- Join the 'albums' and 'songs' tables. (Sort albums from Z-A)
SELECT * FROM songs
   JOIN albums ON albums.id = songs.album_id
   ORDER BY album_title DESC;
